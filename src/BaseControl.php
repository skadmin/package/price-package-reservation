<?php

declare(strict_types=1);

namespace Skadmin\PricePackageReservation;

use App\Model\System\ABaseControl;
use Nette\Utils\ArrayHash;
use Nette\Utils\Html;

class BaseControl extends ABaseControl
{
    public const RESOURCE  = 'price-package-reservation';
    public const DIR_IMAGE = 'price-package-reservation';

    public const PRIVILEGE_TAGS = 'tags';

    public const ADDITIONAL_PRIVILEGE = [
        self::PRIVILEGE_TAGS,
    ];

    public function getMenu() : ArrayHash
    {
        return ArrayHash::from([
            'control' => $this,
            'icon'    => Html::el('i', ['class' => 'fas fa-fw fa-money-bill-wave-alt']),
            'items'   => ['overview'],
        ]);
    }
}
