<?php

declare(strict_types=1);

namespace Skadmin\PricePackageReservation\Doctrine\PricePackageReservation;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Nette\Utils\DateTime;
use SkadminUtils\DoctrineTraits\Entity;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class PricePackageReservationTag
{
    use Entity\Id;
    use Entity\IsActive;
    use Entity\WebalizeName;
    use Entity\Sequence;

    #[ORM\Column]
    private string $color = '';

    #[ORM\ManyToMany(targetEntity: PricePackageReservation::class, mappedBy: 'tags')]
    #[ORM\OrderBy(['termFrom' => 'ASC'])]
    /** @var ArrayCollection|PricePackageReservation[] */
    private array|ArrayCollection|Collection $pricePackageReservations;

    public function __construct()
    {
        $this->pricePackageReservations = new ArrayCollection();
    }

    public function update(string $name, string $color, bool $isActive) : void
    {
        $this->name  = $name;
        $this->color = $color;

        $this->setIsActive($isActive);
    }

    public function getColor() : string
    {
        return $this->color === '' ? '#FFFFFF' : $this->color;
    }

    /**
     * @return ArrayCollection|PricePackageReservation[]
     */
    public function getPricePackageReservations(bool $onlyValid = false, bool $onlyInFuture = false, ?int $limit = null)
    {
        $criteria = Criteria::create();

        if ($onlyValid) {
            $criteria->where(Criteria::expr()->eq('status', PricePackageReservation::STATUS_CONFIRMED));
        }

        if ($onlyInFuture) {
            $criteria->andWhere(Criteria::expr()->gte('termFrom', new DateTime()));
        }

        $criteria->setMaxResults($limit);
        $criteria->orderBy(['termFrom' => 'ASC']);
        return $this->pricePackageReservations->matching($criteria);
    }

    /**
     * Fire trigger every time on update
     *
     * @Doctrine\ORM\Mapping\PreUpdate
     * @Doctrine\ORM\Mapping\PrePersist
     */
    public function onPreUpdateWebalizeName() : void
    {
    }
}
