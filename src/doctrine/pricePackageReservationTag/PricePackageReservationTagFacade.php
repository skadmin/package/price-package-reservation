<?php

declare(strict_types=1);

namespace Skadmin\PricePackageReservation\Doctrine\PricePackageReservation;

use SkadminUtils\DoctrineTraits\Facade;
use App\Model\Doctrine\Traits;
use Nettrine\ORM\EntityManagerDecorator;

final class PricePackageReservationTagFacade extends Facade
{
    use Facade\Webalize;
    use Facade\Sequence;

    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = PricePackageReservationTag::class;
    }

    public function create(string $name, string $color, bool $isActive) : PricePackageReservationTag
    {
        return $this->update(null, $name, $color, $isActive);
    }

    public function update(?int $id, string $name, string $color, bool $isActive) : PricePackageReservationTag
    {
        $PricePackageReservationTag = $this->get($id);
        $PricePackageReservationTag->update($name, $color, $isActive);

        if (! $PricePackageReservationTag->isLoaded()) {
            $PricePackageReservationTag->setWebalize($this->getValidWebalize($name));
            $PricePackageReservationTag->setSequence($this->getValidSequence());
        }

        $this->em->persist($PricePackageReservationTag);
        $this->em->flush();

        return $PricePackageReservationTag;
    }

    public function get(?int $id = null) : PricePackageReservationTag
    {
        if ($id === null) {
            return new PricePackageReservationTag();
        }

        $PricePackageReservationTag = parent::get($id);

        if ($PricePackageReservationTag === null) {
            return new PricePackageReservationTag();
        }

        return $PricePackageReservationTag;
    }

    /**
     * @return PricePackageReservationTag[]
     */
    public function getAll(bool $onlyActive = false) : array
    {
        $criteria = [];

        if ($onlyActive) {
            $criteria['isActive'] = true;
        }

        $orderBy = ['sequence' => 'ASC'];

        return $this->em
            ->getRepository($this->table)
            ->findBy($criteria, $orderBy);
    }

    public function findByWebalize(string $webalize) : ?PricePackageReservationTag
    {
        $criteria = ['webalize' => $webalize];

        return $this->em
            ->getRepository($this->table)
            ->findOneBy($criteria);
    }
}
