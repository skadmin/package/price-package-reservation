<?php

declare(strict_types=1);

namespace Skadmin\PricePackageReservation\Doctrine\PricePackageReservation;

use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\DBAL\Types\Types;
use Skadmin\PricePackage\Doctrine\PricePackage\PricePackage;
use SkadminUtils\DoctrineTraits\Entity;
use Doctrine\ORM\Mapping as ORM;
use function implode;
use function sprintf;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class PricePackageReservation
{
    use Entity\Id;
    use Entity\HumanName;
    use Entity\Address;
    use Entity\Contact;
    use Entity\Content;
    use Entity\Term;
    use Entity\IsActive;
    use Entity\Created;
    use Entity\Status;
    use Entity\Code;
    use Entity\Description;
    use Entity\ImagePreview;
    use Entity\Title;

    public const STATUS_NEW = 1;
    public const STATUS_CONFIRMED = 2;
    public const STATUS_CANCALED = 3;
    public const STATUS = [
        self::STATUS_NEW => 'price-package-reservation.status.new',
        self::STATUS_CONFIRMED => 'price-package-reservation.status.confirmed',
        self::STATUS_CANCALED => 'price-package-reservation.status.cancaled',
    ];

    #[ORM\Column]
    private string $degreeBefore = '';

    #[ORM\Column]
    private string $degreeAfter = '';

    #[ORM\Column]
    private string $addressCountry = '';

    #[ORM\Column]
    private string $companyName = '';

    #[ORM\Column]
    private string $companyRegistrationNumber = '';

    #[ORM\Column]
    private string $companyTaxId = '';

    #[ORM\Column]
    private string $placeOfAction = '';

    #[ORM\Column]
    private int $pricePackagePrice = 0;

    #[ORM\Column(type: Types::TEXT)]
    private string $pricePackageContent = '';

    #[ORM\Column(type: Types::TEXT)]
    private string $note = '';

    #[ORM\ManyToMany(targetEntity: PricePackage::class)]
    #[ORM\JoinTable(name: 'price_package_reservation_rel_price_package')]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    /** @var ArrayCollection<PricePackage>|array<PricePackage> */
    private array|ArrayCollection|Collection $pricePackages;

    #[ORM\ManyToMany(targetEntity: PricePackageReservationTag::class, inversedBy: 'pricePackageReservations')]
    #[ORM\JoinTable(name: 'price_package_reservation_rel_price_package_reservation_tag')]
    #[ORM\OrderBy(['sequence' => 'ASC'])]
    /** @var ArrayCollection<PricePackageReservationTag>|array<PricePackageReservationTag> */
    private array|ArrayCollection|Collection $tags;

    private ?PricePackageReservationTag $firstTag = null;

    public function __construct()
    {
        $this->pricePackages = new ArrayCollection();
        $this->tags = new ArrayCollection();
    }

    /**
     * @param array<PricePackage> $pricePackages
     */
    public function create(string $degreeBefore, string $name, string $surname, string $degreeAfter, string $email, string $phone, string $address, string $addressCountry, string $companyName, string $companyRegistrationNumber, string $companyTaxId, array $pricePackages, DateTimeInterface $termFrom, DateTimeInterface $termTo, string $placeOfAction, string $content, string $note = ''): void
    {
        $this->update(
            $degreeBefore,
            $name,
            $surname,
            $degreeAfter,
            $email,
            $phone,
            $address,
            $addressCountry,
            $companyName,
            $companyRegistrationNumber,
            $companyTaxId,
            $pricePackages,
            $termFrom,
            $termTo,
            $placeOfAction,
        );

        $this->content = $content;
        $this->note = $note;
        $this->description = '';
        $this->title = '';
    }

    /**
     * @param array<PricePackage> $pricePackages
     */
    public function update(string $degreeBefore, string $name, string $surname, string $degreeAfter, string $email, string $phone, string $address, string $addressCountry, string $companyName, string $companyRegistrationNumber, string $companyTaxId, array $pricePackages, DateTimeInterface $termFrom, DateTimeInterface $termTo, string $placeOfAction, int $status = self::STATUS_NEW, string $note = ''): void
    {
        $this->degreeBefore = $degreeBefore;
        $this->name = $name;
        $this->surname = $surname;
        $this->degreeAfter = $degreeAfter;

        $this->email = $email;
        $this->phone = $phone;

        $this->address = $address;
        $this->addressCountry = $addressCountry;

        $this->companyName = $companyName;
        $this->companyRegistrationNumber = $companyRegistrationNumber;
        $this->companyTaxId = $companyTaxId;

        $this->termFrom = $termFrom;
        $this->termTo = $termTo;
        $this->placeOfAction = $placeOfAction;

        $this->pricePackages = $pricePackages;

        $price = 0;
        $content = [];
        foreach ($this->pricePackages as $pricePackage) {
            $price += $pricePackage->getPrice();
            $content[] = sprintf('<div class="mb-3"><h6 class="font-weight-bold mt-1">%s</h6>%s</div>', $pricePackage->getName(), $pricePackage->getContent());
        }
        $this->pricePackagePrice = $price;
        $this->pricePackageContent = implode('', $content);

        $this->status = $status;
        $this->note = $note;
    }

    /**
     * @param array<PricePackageReservationTag> $tags
     */
    public function updatePublic(string $title, string $description, ?string $imagePreview, array $tags): void
    {
        $this->title = $title;
        $this->description = $description;

        $this->tags->clear();
        $this->tags = $tags;

        if ($imagePreview !== null && $imagePreview !== '') {
            $this->imagePreview = $imagePreview;
        }
    }

    public function getDegreeBefore(): string
    {
        return $this->degreeBefore;
    }

    public function getDegreeAfter(): string
    {
        return $this->degreeAfter;
    }

    public function getAddressCountry(): string
    {
        return $this->addressCountry;
    }

    public function getCompanyName(): string
    {
        return $this->companyName;
    }

    public function getCompanyRegistrationNumber(): string
    {
        return $this->companyRegistrationNumber;
    }

    public function getCompanyTaxId(): string
    {
        return $this->companyTaxId;
    }

    public function getPlaceOfAction(): string
    {
        return $this->placeOfAction;
    }

    /**
     * @return ArrayCollection<PricePackage>|array<PricePackage>
     */
    public function getPricePackages()
    {
        return $this->pricePackages;
    }

    /**
     * First "name" OR with $reverse[true] first "surname"
     */
    public function getFullName(bool $reverse = false): string
    {
        if ($reverse) {
            return sprintf('%s %s %s %s', $this->getDegreeBefore(), $this->getSurname(), $this->getName(), $this->getDegreeAfter());
        }
        return sprintf('%s %s %s %s', $this->getDegreeBefore(), $this->getName(), $this->getSurname(), $this->getDegreeAfter());
    }

    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

    public function getStatusText(): string
    {
        return self::STATUS[$this->getStatus()];
    }

    public function getPricePackagePrice(): int
    {
        return $this->pricePackagePrice;
    }

    public function getPricePackageContent(): string
    {
        return $this->pricePackageContent;
    }

    public function getNote(): string
    {
        return $this->note;
    }

    public function setNote(string $note): void
    {
        $this->note = $note;
    }

    /**
     * @return ArrayCollection<PricePackageReservationTag>|array<PricePackageReservationTag>
     */
    public function getTags(bool $onlyActive = false)
    {
        $criteria = Criteria::create();

        if ($onlyActive) {
            $criteria->where(Criteria::expr()->eq('isActive', true));
        }

        return $this->tags->matching($criteria);
    }

    public function getFirstTag(): ?PricePackageReservationTag
    {
        if ($this->firstTag === null) {
            $firstTag = $this->tags->first();
            $this->firstTag = $firstTag ? $firstTag : null;
        }

        return $this->firstTag;
    }

}
