<?php

declare(strict_types=1);

namespace Skadmin\PricePackageReservation\Doctrine\PricePackageReservation;

use Nette\Utils\DateTime;
use SkadminUtils\DoctrineTraits\Facade;
use DateTimeInterface;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Nettrine\ORM\EntityManagerDecorator;
use Skadmin\PricePackage\Doctrine\PricePackage\PricePackage;

final class PricePackageReservationFacade extends Facade
{
    use Facade\Code;

    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = PricePackageReservation::class;
    }

    public function getModelForGrid() : QueryBuilder
    {
        /** @var EntityRepository $repository */
        $repository = $this->em->getRepository($this->table);
        return $repository->createQueryBuilder('a')
            ->leftJoin('a.tags', 't')
            ->orderBy('a.termFrom', 'ASC');
    }

    /**
     * @param PricePackage[]|array $pricePackages
     */
    public function create(string $degreeBefore, string $name, string $surname, string $degreeAfter, string $email, string $phone, string $address, string $addressCountry, string $companyName, string $companyRegistrationNumber, string $companyTaxId, array $pricePackages, DateTimeInterface $termFrom, DateTimeInterface $termTo, string $placeOfAction, string $content, string $note = '') : PricePackageReservation
    {
        $pricePackageReservation = $this->get();
        $pricePackageReservation->create(
            $degreeBefore,
            $name,
            $surname,
            $degreeAfter,
            $email,
            $phone,
            $address,
            $addressCountry,
            $companyName,
            $companyRegistrationNumber,
            $companyTaxId,
            $pricePackages,
            $termFrom,
            $termTo,
            $placeOfAction,
            $content,
            $note
        );

        if (! $pricePackageReservation->isLoaded()) {
            $pricePackageReservation->setCode($this->getValidCode());
        }

        $this->em->persist($pricePackageReservation);
        $this->em->flush();

        return $pricePackageReservation;
    }

    /**
     * @param PricePackage[]|array $pricePackages
     */
    public function update(int $id, string $degreeBefore, string $name, string $surname, string $degreeAfter, string $email, string $phone, string $address, string $addressCountry, string $companyName, string $companyRegistrationNumber, string $companyTaxId, array $pricePackages, DateTimeInterface $termFrom, DateTimeInterface $termTo, string $placeOfAction, int $status, string $note) : PricePackageReservation
    {
        $pricePackageReservation = $this->get($id);
        $pricePackageReservation->update(
            $degreeBefore,
            $name,
            $surname,
            $degreeAfter,
            $email,
            $phone,
            $address,
            $addressCountry,
            $companyName,
            $companyRegistrationNumber,
            $companyTaxId,
            $pricePackages,
            $termFrom,
            $termTo,
            $placeOfAction,
            $status,
            $note
        );

        $this->em->persist($pricePackageReservation);
        $this->em->flush();

        return $pricePackageReservation;
    }

    /**
     * @param array<PricePackageReservationTag> $tags
     */
    public function updatePublic(int $id, string $title, string $description, ?string $imagePreview, array $tags) : PricePackageReservation
    {
        $pricePackageReservation = $this->get($id);
        $pricePackageReservation->updatePublic(
            $title,
            $description,
            $imagePreview,
            $tags
        );

        $this->em->persist($pricePackageReservation);
        $this->em->flush();

        return $pricePackageReservation;
    }

    public function get(?int $id = null) : PricePackageReservation
    {
        if ($id === null) {
            return new PricePackageReservation();
        }

        $pricePackageReservation = parent::get($id);

        if ($pricePackageReservation === null) {
            return new PricePackageReservation();
        }

        return $pricePackageReservation;
    }

    /**
     * @return PricePackageReservation[]
     */
    public function getAll(bool $onlyActive = false) : array
    {
        $criteria = [];

        if ($onlyActive) {
            $criteria['isActive'] = true;
        }

        $orderBy = ['createdAt' => 'DESC'];

        return $this->em
            ->getRepository($this->table)
            ->findBy($criteria, $orderBy);
    }

    /**
     * @param PricePackageReservation|int $pricePackageReservation
     */
    public function updateStatus($pricePackageReservation, int $status) : PricePackageReservation
    {
        if (! $pricePackageReservation instanceof PricePackageReservation) {
            $pricePackageReservation = $this->get($pricePackageReservation);
        }

        $pricePackageReservation->setStatus($status);

        $this->em->persist($pricePackageReservation);
        $this->em->flush();

        return $pricePackageReservation;
    }

    /**
     * @param PricePackageReservation|int $pricePackageReservation
     */
    public function updateIsActive($pricePackageReservation, bool $isActive) : PricePackageReservation
    {
        if (! $pricePackageReservation instanceof PricePackageReservation) {
            $pricePackageReservation = $this->get($pricePackageReservation);
        }

        $pricePackageReservation->setIsActive($isActive);

        $this->em->persist($pricePackageReservation);
        $this->em->flush();

        return $pricePackageReservation;
    }

    /**
     * @return array<PricePackageReservation>
     */
    public function getBetweenDates(DateTimeInterface $dateFrom, DateTimeInterface $dateTo, bool $onlyConfirmed = false, bool $onlyUnarchive = false) : array
    {
        /** @var QueryBuilder $qb */
        $qb = $this->em
            ->getRepository($this->table)
            ->createQueryBuilder('a');

        $qb->select('a')
            ->orderBy('a.termFrom', 'ASC');

        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->gte('a.termFrom', $dateFrom));
        $criteria->andWhere(Criteria::expr()->lte('a.termFrom', $dateTo));

        if ($onlyUnarchive) {
            $criteria->andWhere(Criteria::expr()->eq('a.isActive', true));
        }

        if ($onlyConfirmed) {
            $criteria->andWhere(Criteria::expr()->eq('a.status', PricePackageReservation::STATUS_CONFIRMED));
        }

        $qb->addCriteria($criteria);

        return $qb->getQuery()
            ->getResult();
    }

    /**
     * @return array<int, PricePackageReservation>
     */
    public function getFutureReservations() : array
    {
        $date = new DateTime();
        $date->setTime(0, 0, 0);

        return $this->em
            ->getRepository($this->table)
            ->createQueryBuilder('a')
            ->select('a')
            ->where('a.termTo >= :date')
            ->andWhere('a.status = :status')
            ->andWhere('a.isActive = true')
            ->setParameter('date', $date)
            ->setParameter('status', PricePackageReservation::STATUS_CONFIRMED)
            ->orderBy('a.termTo', 'ASC')
            ->getQuery()
            ->getResult();
    }
}
