<?php

declare(strict_types=1);

namespace Skadmin\PricePackageReservation\Components\Admin;

/**
 * Interface IEditFactory
 */
interface IEditFactory
{
    public function create(?int $id = null) : Edit;
}
