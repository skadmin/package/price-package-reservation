<?php

declare(strict_types=1);

namespace Skadmin\PricePackageReservation\Components\Admin;

use App\Components\Form\FormWithUserControl;
use Skadmin\Role\Doctrine\Role\Privilege;
use App\Model\Mail\MailClassBox;
use App\Model\System\APackageControl;
use App\Model\System\Constant;
use App\Model\System\Flash;
use SkadminUtils\ImageStorage\ImageStorage;
use Defr\Ares;
use Exception;
use Nette\ComponentModel\IContainer;
use Nette\Security\User;
use Nette\Utils\ArrayHash;
use Nette\Utils\Arrays;
use Nette\Utils\DateTime;
use Skadmin\PricePackage\Doctrine\PricePackage\PricePackage;
use Skadmin\PricePackage\Doctrine\PricePackage\PricePackageFacade;
use Skadmin\PricePackageReservation\BaseControl;
use Skadmin\PricePackageReservation\Doctrine\PricePackageReservation\PricePackageReservation;
use Skadmin\PricePackageReservation\Doctrine\PricePackageReservation\PricePackageReservationFacade;
use Skadmin\PricePackageReservation\Doctrine\PricePackageReservation\PricePackageReservationTag;
use Skadmin\PricePackageReservation\Doctrine\PricePackageReservation\PricePackageReservationTagFacade;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use SkadminUtils\FormControls\Utils\UtilsFormControl;
use WebLoader\Nette\CssLoader;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;
use function explode;
use function is_bool;
use function method_exists;
use function sprintf;
use function trim;

/**
 * Class Edit
 */
class Edit extends FormWithUserControl
{
    use APackageControl;

    /** @var LoaderFactory */
    public $webLoader;

    /** @var PricePackageReservationFacade */
    private $facade;

    /** @var PricePackageFacade */
    private $facadePricePackage;

    /** @var PricePackageReservationTagFacade */
    private $facadePricePackageReservationTag;

    /** @var PricePackageReservation */
    private $pricePackageReservation;

    /** @var MailClassBox */
    private $mailClassBox;

    /** @var ImageStorage */
    private $imageStorage;

    public function __construct(?int $id, PricePackageReservationFacade $facade, PricePackageFacade $facadePricePackage, PricePackageReservationTagFacade $facadePricePackageReservationTag, Translator $translator, LoaderFactory $webLoader, User $user, MailClassBox $mailClassBox, ImageStorage $imageStorage)
    {
        parent::__construct($translator, $user);
        $this->facade = $facade;
        $this->facadePricePackage = $facadePricePackage;
        $this->facadePricePackageReservationTag = $facadePricePackageReservationTag;

        $this->webLoader = $webLoader;
        $this->mailClassBox = $mailClassBox;
        $this->imageStorage = $imageStorage;

        $this->pricePackageReservation = $this->facade->get($id);
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (!$this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    /**
     * @return SimpleTranslation|string
     */
    public function getTitle()
    {
        if ($this->pricePackageReservation->isLoaded()) {
            return new SimpleTranslation('price-package-reservation.edit.title - %s', $this->pricePackageReservation->getFullName());
        }

        return 'price-package-reservation.edit.title';
    }

    /**
     * @return CssLoader[]
     */
    public function getCss(): array
    {
        return [
            $this->webLoader->createCssLoader('customFileInput'),
            $this->webLoader->createCssLoader('daterangePicker'),
            $this->webLoader->createCssLoader('fancyBox'), // responsive file manager
        ];
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [
            $this->webLoader->createJavaScriptLoader('adminTinyMce'),
            $this->webLoader->createJavaScriptLoader('moment'),
            $this->webLoader->createJavaScriptLoader('daterangePicker'),
            $this->webLoader->createJavaScriptLoader('fancyBox'), // responsive file manager
        ];
    }

    public function processOnSuccess(Form $form, ArrayHash $values): void
    {
        /**
         * @var DateTime $termFrom
         * @var DateTime $termTo
         */
        [$termFrom, $termTo] = Arrays::map(explode(' - ', $values->term), static function (string $date): DateTime {
            $date = DateTime::createFromFormat('d.m.Y H:i', $date);
            return is_bool($date) ? new DateTime() : $date;
        });

        // PRICE PACKAGES
        $pricePackages = [];
        foreach ($values->price_package_id as $pricePackageId) {
            $pricePackages[] = $this->facadePricePackage->get($pricePackageId);
        }

        // IDENTIFIER
        $identifier = UtilsFormControl::getImagePreview($values->image_preview, BaseControl::DIR_IMAGE);

        if ($identifier !== null && $this->pricePackageReservation->getImagePreview() !== null) {
            $this->imageStorage->delete($this->pricePackageReservation->getImagePreview());
        }

        // TAGS
        if (isset($values->tags)) {
            $tags = Arrays::map($values->tags, function ($tagId): PricePackageReservationTag {
                return $this->facadePricePackageReservationTag->get($tagId);
            });
        }

        if ($this->pricePackageReservation->isLoaded()) {
            $pricePackageReservation = $this->facade->update(
                $this->pricePackageReservation->getId(),
                $values->degree_before,
                $values->name,
                $values->surname,
                $values->degree_after,
                $values->email,
                $values->phone,
                $values->address,
                $values->address_country,
                $values->is_company ? $values->company_name : '',
                $values->is_company ? $values->company_registration_number : '',
                $values->is_company ? $values->company_tax_id : '',
                $pricePackages,
                $termFrom,
                $termTo,
                $values->place_of_action,
                $values->status,
                $values->note
            );
        } else {
            $pricePackageReservation = $this->facade->create(
                $values->degree_before,
                $values->name,
                $values->surname,
                $values->degree_after,
                $values->email,
                $values->phone,
                $values->address,
                $values->address_country,
                $values->is_company ? $values->company_name : '',
                $values->is_company ? $values->company_registration_number : '',
                $values->is_company ? $values->company_tax_id : '',
                $pricePackages,
                $termFrom,
                $termTo,
                $values->place_of_action,
                '',
                $values->note
            );

            $this->facade->updateStatus($pricePackageReservation, $values->status);
        }

        $pricePackageReservation = $this->facade->updatePublic(
            $pricePackageReservation->getId(),
            $values->title,
            $values->description,
            $identifier,
            $tags ?? $this->pricePackageReservation->getTags()->toArray()
        );

        $this->onFlashmessage('form.price-package-reservation.edit.flash.success.update', Flash::SUCCESS);

        if ($values->sendEmail) {
            if (!method_exists($this->mailClassBox, 'overviewRegistrationUserSendEmailUpdate')) {
                throw new Exception('Mail class box not have method "overviewRegistrationUserSendEmailUpdate"');
            } elseif ($this->mailClassBox->overviewRegistrationUserSendEmailUpdate($pricePackageReservation)) {
                $message = new SimpleTranslation('form.price-package-reservation.edit.flash.registration-update.success %s', $pricePackageReservation->getEmail());
                $this->onFlashmessage($message, Flash::SUCCESS);
            } else {
                $message = new SimpleTranslation('form.price-package-reservation.edit.flash.registration-update.danger %s', $pricePackageReservation->getEmail());
                $this->onFlashmessage($message, Flash::DANGER);
            }
        }

        if ($form->isSubmitted()->name === 'send_back') {
            $this->processOnBack();
        }

        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render' => 'edit',
            'id' => $pricePackageReservation->getId(),
        ]);
    }

    public function processOnBack(): void
    {
        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render' => 'overview',
        ]);
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/edit.latte');
        $template->pricePackageReservation = $this->pricePackageReservation;
        $template->render();
    }

    protected function createComponentForm(): Form
    {
        $form = new Form();
        $form->setTranslator($this->translator);

        // DATA
        $dataFormPricePackage = [];
        $dataFromPricePackageInactive = [];
        foreach ($this->facadePricePackage->getAll() as $pricePackage) {
            if (!$pricePackage->isActive()){
                $dataFromPricePackageInactive[$pricePackage->getId()] = $pricePackage->getName();
                continue;
            }

            $dataFormPricePackage[$pricePackage->getId()] = $pricePackage->getName();
        }

        foreach ($dataFromPricePackageInactive as $key => $value) {
            $dataFormPricePackage[$key] = sprintf('[neaktivní] %s', $value);
        }

        // INPUT

        // base
        $form->addText('title', 'form.price-package-reservation.edit.title');
        $form->addTextArea('description', 'form.price-package-reservation.edit.description');
        $form->addTextArea('note', 'form.price-package-reservation.edit.note');
        $form->addImageWithRFM('image_preview', 'form.price-package-reservation.edit.image-preview');

        if ($this->isAllowed(BaseControl::RESOURCE, BaseControl::PRIVILEGE_TAGS)) {
            $form->addMultiSelect('tags', 'form.price-package-reservation.edit.tags', $this->facadePricePackageReservationTag->getPairs('id', 'name'))
                ->setTranslator(null);
        }

        // Customer
        $form->addText('degree_before', 'form.price-package-reservation.edit.degree-before');
        $form->addText('name', 'form.price-package-reservation.edit.name')
            ->setRequired('form.price-package-reservation.edit.name.req');
        $form->addText('surname', 'form.price-package-reservation.edit.surname')
            ->setRequired('form.price-package-reservation.edit.surname.req');
        $form->addText('degree_after', 'form.price-package-reservation.edit.degree-after');

        $form->addEmail('email', 'form.price-package-reservation.edit.email')
            ->setRequired('form.price-package-reservation.edit.email.req');
        $form->addText('phone', 'form.price-package-reservation.edit.phone')
            ->setRequired('form.price-package-reservation.edit.phone.req');

        $form->addText('address', 'form.price-package-reservation.edit.address')
            ->setRequired('form.price-package-reservation.edit.address.req');
        $form->addText('address_country', 'form.price-package-reservation.edit.address-country')
            ->setRequired('form.price-package-reservation.edit.address-country.req');

        // Company
        $inputIsCompany = $form->addCheckbox('is_company', 'form.price-package-reservation.edit.is-company');
        $inputIsCompany->addCondition(Form::EQUAL, true)
            ->toggle('form-company');
        $form->addText('company_name', 'form.price-package-reservation.edit.company-name')
            ->addConditionOn($inputIsCompany, Form::EQUAL, true)
            ->setRequired('form.price-package-reservation.edit.company-name.req');
        $form->addText('company_registration_number', 'form.price-package-reservation.edit.company-registration-number')
            ->setHtmlAttribute('placeholder', 'form.price-package-reservation.edit.phone.placeholder')
            ->addConditionOn($inputIsCompany, Form::EQUAL, true)
            ->setRequired('form.price-package-reservation.edit.company-registration-number.req');
        $form->addText('company_tax_id', 'form.price-package-reservation.edit.company-tax-id');

        // Package
        $form->addSelect('status', 'form.price-package-reservation.edit.status', PricePackageReservation::STATUS)
            ->setPrompt(Constant::PROMTP);
        $form->addMultiSelect('price_package_id', 'form.price-package-reservation.edit.price-package-id', $dataFormPricePackage)
            ->setTranslator(null)
            ->setRequired('form.price-package-reservation.edit.price-package-id.req');
        $form->addText('term', 'form.price-package-reservation.edit.term')
            ->setHtmlAttribute('data-daterange', 'DD.MM.YYYY HH:mm')
            ->setHtmlAttribute('data-daterange-timepicker');
        $form->addText('place_of_action', 'form.price-package-reservation.edit.place-of-action');

        $form->addCheckbox('sendEmail', 'form.price-package-reservation.edit.send-email');

        // BUTTON
        $form->addSubmit('send', 'form.price-package-reservation.edit.send');
        $form->addSubmit('send_back', 'form.price-package-reservation.edit.send-back');
        $form->addSubmit('back', 'form.price-package-reservation.edit.back')
            ->setValidationScope([])
            ->onClick[] = [$this, 'processOnBack'];

        // DEFAULT
        $form->setDefaults($this->getDefaults());

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }

    public function handleLoadFromAres(?string $val = null): void
    {
        try {
            $ares = (new Ares())->findByIdentificationNumber($val);

            $this['form']['company_name']->setValue($ares->getCompanyName());
            $this['form']['company_registration_number']->setValue($ares->getCompanyId());
            $this['form']['company_tax_id']->setValue($ares->getTaxId());

            $this->redrawControl('snipForm');
            $this->redrawControl('snipFormcompany');
        } catch (Ares\AresException $e) {
            $presenter = $this->getPresenterIfExists();
            if ($presenter !== null) {
                $presenter->flashMessage('form.price-package-reservation.edit.flash.info.invalid-company-id', Flash::WARNING);
            }
        }
    }

    /**
     * @return mixed[]
     */
    private function getDefaults(): array
    {
        if (!$this->pricePackageReservation->isLoaded()) {
            return [];
        }

        $tagsId = Arrays::map($this->pricePackageReservation->getTags()->toArray(), static function (PricePackageReservationTag $tag): ?int {
            return $tag->getId();
        });

        $pricePackagesId = Arrays::map($this->pricePackageReservation->getPricePackages()->toArray(), static function (PricePackage $pricePackage): ?int {
            return $pricePackage->getId();
        });

        return [
            // base
            'title' => $this->pricePackageReservation->getTitle(),
            'note' => $this->pricePackageReservation->getNote(),
            'description' => $this->pricePackageReservation->getDescription(),
            'tags' => $tagsId,
            // other...
            'degree_before' => $this->pricePackageReservation->getDegreeBefore(),
            'name' => $this->pricePackageReservation->getName(),
            'surname' => $this->pricePackageReservation->getSurname(),
            'degree_after' => $this->pricePackageReservation->getDegreeAfter(),
            'email' => $this->pricePackageReservation->getEmail(),
            'phone' => $this->pricePackageReservation->getPhone(),
            'address' => $this->pricePackageReservation->getAddress(),
            'address_country' => $this->pricePackageReservation->getAddressCountry(),
            'is_company' => trim($this->pricePackageReservation->getCompanyName()) !== '',
            'company_name' => $this->pricePackageReservation->getCompanyName(),
            'company_registration_number' => $this->pricePackageReservation->getCompanyRegistrationNumber(),
            'company_tax_id' => $this->pricePackageReservation->getCompanyTaxId(),
            'status' => $this->pricePackageReservation->getStatus(),
            'price_package_id' => $pricePackagesId,
            'term' => sprintf(
                '%s - %s',
                $this->pricePackageReservation->getTermFrom()->format('d.m.Y H:i'),
                $this->pricePackageReservation->getTermTo()->format('d.m.Y H:i')
            ),
            'place_of_action' => $this->pricePackageReservation->getPlaceOfAction(),
        ];
    }
}
