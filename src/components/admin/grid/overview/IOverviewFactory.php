<?php

declare(strict_types=1);

namespace Skadmin\PricePackageReservation\Components\Admin;

/**
 * Interface IOverviewFactory
 */
interface IOverviewFactory
{
    public function create() : Overview;
}
