<?php

declare(strict_types=1);

namespace Skadmin\PricePackageReservation\Components\Admin;

use SkadminUtils\GridControls\UI\GridControl;
use SkadminUtils\GridControls\UI\GridDoctrine;
use Skadmin\Role\Doctrine\Role\Privilege;
use App\Model\Mail\MailClassBox;
use App\Model\System\APackageControl;
use App\Model\System\Constant;
use App\Model\System\Flash;
use App\Model\System\Utils;
use Exception;
use Nette\ComponentModel\IContainer;
use Nette\Security\User;
use Nette\Utils\Arrays;
use Nette\Utils\Html;
use Skadmin\PricePackage\Doctrine\PricePackage\PricePackageFacade;
use Skadmin\PricePackageReservation\BaseControl;
use Skadmin\PricePackageReservation\Doctrine\PricePackageReservation\PricePackageReservation;
use Skadmin\PricePackageReservation\Doctrine\PricePackageReservation\PricePackageReservationFacade;
use Skadmin\PricePackageReservation\Doctrine\PricePackageReservation\PricePackageReservationTagFacade;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\Utils\Utils\Colors\Colors;
use function implode;
use function intval;
use function method_exists;
use function number_format;
use function sprintf;

/**
 * Class Overview
 */
class Overview extends GridControl
{
    use APackageControl;

    public const ARCHIVE   = 0;
    public const UNARCHIVE = 1;

    public const STATUS_ARCHIVE = [
        self::UNARCHIVE => 'grid.price-package-reservation.overview.status.unarchive',
        self::ARCHIVE   => 'grid.price-package-reservation.overview.status.archive',
    ];

    /** @var PricePackageReservationFacade */
    private $facade;

    /** @var PricePackageReservationTagFacade */
    private $facadePricePackageReservationTag;

    /** @var PricePackageFacade */
    private $facadePricePackage;

    /** @var MailClassBox */
    private $mailClassBox;

    public function __construct(PricePackageReservationFacade $facade, PricePackageReservationTagFacade $facadePricePackageReservationTag, PricePackageFacade $facadePricePackage, Translator $translator, MailClassBox $mailClassBox, User $user)
    {
        parent::__construct($translator, $user);

        $this->facade                           = $facade;
        $this->facadePricePackageReservationTag = $facadePricePackageReservationTag;
        $this->facadePricePackage               = $facadePricePackage;
        $this->mailClassBox                     = $mailClassBox;
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::READ)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function render() : void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/overview.latte');
        $template->render();
    }

    public function getTitle() : string
    {
        return 'price-package-reservation.overview.title';
    }

    protected function createComponentGrid(string $name) : GridDoctrine
    {
        $grid = new GridDoctrine($this->getPresenter());

        // DEFAULT
        $grid->setPrimaryKey('id');
        $grid->setDataSource($this->facade->getModelForGrid());

        // DATA
        $dataStatus    = Arrays::map(PricePackageReservation::STATUS, function ($status) : string {
            return $this->translator->translate($status);
        });
        $dataStatusBtn = [
            PricePackageReservation::STATUS_NEW       => 'btn-light',
            PricePackageReservation::STATUS_CANCALED  => 'btn-danger',
            PricePackageReservation::STATUS_CONFIRMED => 'btn-success',
        ];

        $dataStatusArciveBtn = [
            self::UNARCHIVE => 'btn-light',
            self::ARCHIVE => 'btn-danger',
        ];

        $dataPricePackages = $this->facadePricePackage->getPairs('id', 'name');

        $dataArchive = Arrays::map(self::STATUS_ARCHIVE, function ($item) : string {
            return $this->translator->translate($item);
        });

        // COLUMNS
        $grid->addColumnText('name', 'grid.price-package-reservation.overview.name', 'fullName')
            ->setRenderer(function (PricePackageReservation $pricePackageReservation) : Html {
                $code = Html::el('code', ['class' => 'text-muted small'])
                    ->setText($pricePackageReservation->getCode());

                if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
                    $link = $this->getPresenter()->link('Component:default', [
                        'package' => new BaseControl(),
                        'render'  => 'edit',
                        'id'      => $pricePackageReservation->getId(),
                    ]);

                    $name = Html::el('a', [
                        'href'  => $link,
                        'class' => 'font-weight-bold',
                    ]);
                } else {
                    $name = new Html();
                }
                $name->setText($pricePackageReservation->getFullName());

                $result = new Html();
                $result->addHtml($name)
                    ->addHtml('<br>')
                    ->addHtml($code);

                return $result;
            });
        $grid->addColumnText('pricePackage', 'grid.price-package-reservation.overview.price-package', 'pricePackage.name')
            ->setRenderer(static function (PricePackageReservation $pricePackageReservation) : Html {
                $packages = new Html();
                foreach ($pricePackageReservation->getPricePackages() as $package) {
                    $packageRow = Html::el('div', ['class' => 'row'])
                        ->addHtml(Html::el('div', ['class' => 'col-7'])
                            ->setText($package->getName()))
                        ->addHtml(Html::el('div', ['class' => 'col-5 text-right'])
                            ->setText(sprintf('%s,- Kč', number_format($package->getPrice(), 0, ',', ' '))));
                    $packages->addHtml($packageRow);
                }

                $supplementsIcon = Html::el('i', [
                    'class'          => 'far fa-sm fa-question-circle text-primary mr-2',
                    'data-toggle'    => 'popover',
                    'data-content'   => $packages,
                    'data-placement' => 'left',
                    'data-html'      => 'true',
                    'data-trigger'   => 'click hover',
                ]);

                $price = Html::el('span', ['class' => 'font-weight-bold text-nowrap']);
                $price->setHtml($supplementsIcon)
                    ->addText(sprintf('%s,- Kč', number_format($pricePackageReservation->getPricePackagePrice(), 0, ',', ' ')));

                $render = new Html();
                $render->addHtml($price);

                return $render;
            });
        $grid->addColumnText('contact', 'grid.price-package-reservation.overview.contact')
            ->setRenderer(static function (PricePackageReservation $pricePackageReservation) : Html {
                $contact = new Html();

                $contacts = [];
                if ($pricePackageReservation->getEmail() !== '') {
                    $contacts[] = Utils::createHtmlContact($pricePackageReservation->getEmail());
                }

                if ($pricePackageReservation->getPhone() !== '') {
                    $contacts[] = Utils::createHtmlContact($pricePackageReservation->getPhone());
                }

                $contact->addHtml(implode('<br>', $contacts));

                return $contact;
            });
        if ($this->isAllowed(BaseControl::RESOURCE, BaseControl::PRIVILEGE_TAGS)) {
            $grid->addColumnText('tags', 'grid.price-package-reservation.overview.tags')
                ->setRenderer(static function (PricePackageReservation $pricePackageReservation) : Html {
                    $tags = new Html();
                    foreach ($pricePackageReservation->getTags() as $tag) {
                        $color = $tag->getColor() !== '' ? $tag->getColor() : '#FFFFFF';
                        $tags->addHtml(Html::el('span', [
                            'class' => 'badge mr-1 px-2 mb-1 border border-primary',
                            'style' => sprintf('background-color: %s; color: %s', $color, Colors::getContrastColor($color)),
                        ])->setText($tag->getName()));
                    }
                    return $tags;
                });
        }
        $grid->addColumnText('term', 'grid.price-package-reservation.overview.term', 'termWithTimeClever')
            ->setAlign('center');
        $grid->addColumnDateTime('createdAt', 'grid.price-package-reservation.overview.created-at')
            ->setFormat('d.m.Y H:i');
        $griColumnIsActive = $grid->addColumnStatus('isActive', 'grid.price-package-reservation.overview.is-active')
            ->setAlign('center')
            ->setCaret(false);
        foreach (self::STATUS_ARCHIVE as $statusId => $status) {
            \Tracy\Debugger::barDump([$statusId, $status]);
            $griColumnIsActive
                ->addOption(boolval($statusId), $status)
                ->setClass($dataStatusArciveBtn[$statusId])
                ->endOption();
        }
        $griColumnIsActive->onChange[] = [$this, 'overviewPricePackageRegistrationsOnChangeIsActive'];

        $griColumnStatus = $grid->addColumnStatus('status', 'grid.price-package-reservation.overview.status')
            ->setAlign('center')
            ->setCaret(false);
        foreach (PricePackageReservation::STATUS as $statusId => $status) {
            $griColumnStatus
                ->addOption($statusId, $status)
                ->setClass($dataStatusBtn[$statusId])
                ->endOption();
        }
        $griColumnStatus->onChange[] = [$this, 'overviewPricePackageRegistrationsOnChangeStatus'];

        // FILTER
        $grid->addFilterText('name', 'grid.price-package-reservation.overview.name', ['degreeBefore', 'name', 'surname', 'degreeAfter', 'code']);
        $grid->addFilterSelect('pricePackage', 'grid.price-package-reservation.overview.name', $dataPricePackages)
            ->setPrompt(Constant::PROMTP);
        $grid->addFilterSelect('isActive', 'grid.price-package-reservation.overview.is-active', $dataArchive)
            ->setPrompt(Constant::PROMTP);
        $grid->addFilterSelect('status', 'grid.price-package-reservation.overview.status', $dataStatus)
            ->setPrompt(Constant::PROMTP);
        if ($this->isAllowed(BaseControl::RESOURCE, BaseControl::PRIVILEGE_TAGS)) {
            $grid->addFilterSelect('tags', 'grid.price-package-reservation.overview.tags', $this->facadePricePackageReservationTag->getPairs('id', 'name'), 't.id')
                ->setPrompt(Constant::PROMTP);
        }

        // DETAIL
        $grid->setItemsDetail(__DIR__ . '/detail.latte');

        // ACTION
        if ($this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $grid->addAction('edit', 'grid.price-package-reservation.overview.action.edit', 'Component:default', ['id' => 'id'])->addParameters([
                'package' => new BaseControl(),
                'render'  => 'edit',
            ])->setIcon('pencil-alt')
                ->setClass('btn btn-xs btn-default btn-primary');
        }

        // TOOLBAR
        if ($this->isAllowed(BaseControl::RESOURCE, BaseControl::PRIVILEGE_TAGS)) {
            $grid->addToolbarButton('Component:default#2', 'grid.price-package-reservation.overview.action.overview-tag', [
                'package' => new BaseControl(),
                'render'  => 'overview-tag',
            ])->setIcon('tags')
                ->setClass('btn btn-xs btn-outline-primary');

            $grid->addToolbarButton('Component:default', 'grid.price-package-reservation.overview.action.new', [
                'package'  => new BaseControl(),
                'render'   => 'edit',
            ])->setIcon('plus')
                ->setClass('btn btn-xs btn-primary');
        }

        // OTHER
        $grid->setDefaultSort(['createdAt' => 'DESC']);
        $grid->setDefaultFilter([
            'isActive' => self::UNARCHIVE,
        ]);

        return $grid;
    }

    public function overviewPricePackageRegistrationsOnChangeStatus(string $id, string $newStatus) : void
    {
        $id        = intval($id);
        $newStatus = intval($newStatus);

        $pricePackageReservation = $this->facade->updateStatus($id, $newStatus);

        $presenter = $this->getPresenterIfExists();
        if ($presenter !== null) {
            $newStatusText = $this->translator->translate($pricePackageReservation->getStatusText());
            $message       = new SimpleTranslation('grid.price-package-reservation.overview.flash.change-status %s', $newStatusText);
            $presenter->flashMessage($this->translator->translate($message), Flash::SUCCESS);
        }

        if (! method_exists($this->mailClassBox, 'overviewRegistrationUserSendEmailUpdate')) {
            throw new Exception('Mail class box not have method "overviewRegistrationUserSendEmailUpdate"');
        } elseif ($this->mailClassBox->overviewRegistrationUserSendEmailUpdate($pricePackageReservation)) {
            $message = new SimpleTranslation('grid.price-package-reservation.overview.flash.registration-update.success %s', $pricePackageReservation->getEmail());
            $presenter->flashMessage($this->translator->translate($message), Flash::SUCCESS);
        } else {
            $message = new SimpleTranslation('grid.price-package-reservation.overview.flash.registration-update.danger %s', $pricePackageReservation->getEmail());
            $presenter->flashMessage($this->translator->translate($message), Flash::DANGER);
        }

        $this['grid']->redrawItem($id);
    }


    public function overviewPricePackageRegistrationsOnChangeIsActive(string $id, string $isActive): void
    {
        $id = intval($id);
        $isActive = boolval(intval($isActive));

        $pricePackageReservation = $this->facade->updateIsActive($id, $isActive);

        $presenter = $this->getPresenterIfExists();
        if ($presenter !== null) {
            $newStatusText = $this->translator->translate(self::STATUS_ARCHIVE[intval($isActive)]);
            $message = new SimpleTranslation('grid.price-package-reservation.overview.flash.change-is-active %s', $newStatusText);
            $presenter->flashMessage($this->translator->translate($message), Flash::SUCCESS);
        }

        $this['grid']->redrawItem($id);
    }
}
