<?php

declare(strict_types=1);

namespace Skadmin\PricePackageReservation\Components\Admin;

/**
 * Interface IOverviewTagFactory
 */
interface IOverviewTagFactory
{
    public function create() : OverviewTag;
}
