<?php

declare(strict_types=1);

namespace Skadmin\PricePackageReservation\Components\Admin;

use SkadminUtils\GridControls\UI\GridControl;
use SkadminUtils\GridControls\UI\GridDoctrine;
use Skadmin\Role\Doctrine\Role\Privilege;
use App\Model\Grid\Traits\IsActive;
use App\Model\System\APackageControl;
use App\Model\System\Constant;
use App\Model\System\Flash;
use Nette\ComponentModel\IContainer;
use Nette\Forms\Container;
use Nette\Security\User;
use Nette\Utils\ArrayHash;
use Nette\Utils\Arrays;
use Nette\Utils\Html;
use Skadmin\PricePackageReservation\BaseControl;
use Skadmin\PricePackageReservation\Doctrine\PricePackageReservation\PricePackageReservationTag;
use Skadmin\PricePackageReservation\Doctrine\PricePackageReservation\PricePackageReservationTagFacade;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use WebLoader\Nette\CssLoader;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;
use function boolval;
use function intval;

/**
 * Class Overview
 */
class OverviewTag extends GridControl
{
    use APackageControl;
    use IsActive;

    /** @var PricePackageReservationTagFacade */
    private $facade;

    /** @var LoaderFactory */
    private $webLoader;

    public function __construct(PricePackageReservationTagFacade $facade, Translator $translator, User $user, LoaderFactory $webLoader)
    {
        parent::__construct($translator, $user);

        $this->facade    = $facade;
        $this->webLoader = $webLoader;
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, BaseControl::PRIVILEGE_TAGS)) {
            $this->onFlashmessage('grid.price-package-reservation.overview-tag.name.flash.info.denide-acccess-tags', Flash::INFO);
            $this->getPresenter()->redirect('Component:default', [
                'package' => new BaseControl(),
                'render'  => 'overview',
            ]);
        }

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::READ)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function render() : void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/overviewTag.latte');
        $template->render();
    }

    public function getTitle() : string
    {
        return 'price-package-reservation.overview-tag.title';
    }

    /**
     * @return CssLoader[]
     */
    public function getCss() : array
    {
        return [
            $this->webLoader->createCssLoader('colorPicker'),
        ];
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs() : array
    {
        return [
            $this->webLoader->createJavaScriptLoader('jQueryUi'),
            $this->webLoader->createJavaScriptLoader('colorPicker'),
        ];
    }

    protected function createComponentGrid(string $name) : GridDoctrine
    {
        $grid = new GridDoctrine($this->getPresenter());

        // DEFAULT
        $grid->setPrimaryKey('id');
        $grid->setDataSource($this->facade->getModel()
            ->orderBy('a.sequence', 'ASC'));

        // DATA
        $translator = $this->translator;
        $dialYesNo  = Arrays::map(Constant::DIAL_YES_NO, static function ($text) use ($translator) : string {
            return $translator->translate($text);
        });

        // COLUMNS
        $grid->addColumnText('name', 'grid.price-package-reservation.overview-tag.name')
            ->setRenderer(function (PricePackageReservationTag $pricePackageReservationTag) : Html {
                if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
                    $link = $this->getPresenter()->link('Component:default', [
                        'package' => new BaseControl(),
                        'render'  => 'edit-tag',
                        'id'      => $pricePackageReservationTag->getId(),
                    ]);

                    $name = Html::el('a', [
                        'href'  => $link,
                        'class' => 'font-weight-bold',
                    ]);
                } else {
                    $name = new Html();
                }

                $name->setText($pricePackageReservationTag->getName());

                return $name;
            });
        $grid->addColumnText('color', 'grid.price-package-reservation.overview-tag.color')
            ->setRenderer(static function (PricePackageReservationTag $pricePackageReservationTag) : Html {
                return Html::el('span', ['data-color-view' => 'color'])
                    ->setText($pricePackageReservationTag->getColor());
            })->setAlign('center');
        $this->addColumnIsActive($grid, 'price-package-reservation.overview-tag');

        // FILTER
        $grid->addFilterText('name', 'grid.price-package-reservation.overview-tag.name');
        $this->addFilterIsActive($grid, 'price-package-reservation.overview');

        // ACTION
        if ($this->isAllowed(BaseControl::RESOURCE, BaseControl::PRIVILEGE_TAGS) && $this->isAllowed(BaseControl::RESOURCE, 'write')) {
            // INLINE ADD
            $grid->addInlineAdd()
                ->setPositionTop()
                ->setText($this->translator->translate('grid.price-package-reservation.overview-tag.overview.action.inline-add'))
                ->onControlAdd[] = [$this, 'onInlineAdd'];
            $grid->getInlineAddPure()
                ->onSubmit[]     = [$this, 'onInlineAddSubmit'];

            // INLINE EDIT
            $grid->addInlineEdit()
                ->onControlAdd[]                        = [$this, 'onInlineEdit'];
            $grid->getInlineEditPure()->onSetDefaults[] = [$this, 'onInlineEditDefaults'];
            $grid->getInlineEditPure()->onSubmit[]      = [$this, 'onInlineEditSubmit'];
        }

        // TOOLBAR
        $grid->addToolbarButton('Component:default#2', 'grid.price-package-reservatio.overview-tag.action.overview', [
            'package' => new BaseControl(),
            'render'  => 'overview',
        ])->setIcon('list-ul')
            ->setClass('btn btn-xs btn-outline-primary');

        // ALLOW

        // SORTING
        $grid->setSortable();
        $grid->setSortableHandler($this->link('sort!'));

        return $grid;
    }

    public function onInlineAdd(Container $container) : void
    {
        $this->initFormContainer($container);
    }

    private function initFormContainer(Container $container) : Container
    {
        $container->addText('name', 'grid.price-package-reservation.overview-tag.overview.name')
            ->setRequired('grid.price-package-reservation.overview-tag.overview.name.req');
        $container->addInlineArray('additionalSettings', 'grid.price-package-reservation.overview-tag.overview.additional-settings');
        $container->addText('color', 'grid.price-package-reservation.overview-tag.overview.color')
            ->setHtmlAttribute('data-create-colorpicker')
            ->setHtmlAttribute('data-format', 'hex');
        $container->addSelect('isActive', 'grid.price-package-reservation.overview-tag.overview.is-active', Constant::DIAL_YES_NO)
            ->setDefaultValue(Constant::YES);

        return $container;
    }

    /**
     * @param ArrayHash<mixed> $values
     */
    public function onInlineAddSubmit(ArrayHash $values) : void
    {
        $pricePackageReservationTag = $this->facade->create($values->name, $values->color, boolval($values->isActive));

        $message = new SimpleTranslation('grid.price-package-reservation.overview-tag.overview.action.flash.inline-add.success "%s"', [$pricePackageReservationTag->getName()]);
        $this->onFlashmessage($message, Flash::SUCCESS);
    }

    public function onInlineEdit(Container $container) : void
    {
        $this->initFormContainer($container);
    }

    public function onInlineEditDefaults(Container $container, PricePackageReservationTag $pricePackageReservationTag) : void
    {
        $container->setDefaults([
            'name'     => $pricePackageReservationTag->getName(),
            'color'    => $pricePackageReservationTag->getColor(),
            'isActive' => intval($pricePackageReservationTag->isActive()),
        ]);
    }

    /**
     * @param int|string $id
     * @param ArrayHash<mixed> $values
     */
    public function onInlineEditSubmit($id, ArrayHash $values) : void
    {
        $pricePackageReservationTag = $this->facade->update(intval($id), $values->name, $values->color, boolval($values->isActive));

        $message = new SimpleTranslation('grid.price-package-reservation.overview-tag.overview.action.flash.inline-edit.success "%s"', [$pricePackageReservationTag->getName()]);
        $this->onFlashmessage($message, Flash::SUCCESS);
    }

    public function handleSort(?string $item_id, ?string $prev_id, ?string $next_id) : void
    {
        $this->facade->sort($item_id, $prev_id, $next_id);

        $presenter = $this->getPresenterIfExists();
        if ($presenter !== null) {
            $presenter->flashMessage('grid.price-package-reservation.overview-tag.action.flash.sort.success', Flash::SUCCESS);
        }

        $this['grid']->reload();
    }
}
