<?php

declare(strict_types=1);

namespace Skadmin\PricePackageReservation\Components\Front;

/**
 * Interface IEditFactory
 */
interface IFormCreateReservationFactory
{
    /**
     * @param int|string|null $defaultPackageId
     * @param int|string|null $defaultTagId
     */
    public function create($defaultPackageId = null, $defaultTagId = null, bool $multiplePackage = false) : FormCreateReservation;
}
