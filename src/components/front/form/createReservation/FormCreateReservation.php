<?php

declare(strict_types=1);

namespace Skadmin\PricePackageReservation\Components\Front;

use App\Components\Form\FormWithUserControl;
use h4kuna\Ares\AresFactory;
use Nette\Forms\Controls\TextInput;
use Skadmin\Mailing\Doctrine\Mail\MailQueue;
use Skadmin\Mailing\Model\MailService;
use App\Model\System\APackageControl;
use App\Model\System\Constant;
use App\Model\System\Flash;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Nette\Utils\Arrays;
use Nette\Utils\DateTime;
use Nette\Utils\Html;
use Skadmin\PricePackage\Doctrine\PricePackage\PricePackageFacade;
use Skadmin\PricePackageReservation\Doctrine\PricePackageReservation\PricePackageReservationFacade;
use Skadmin\PricePackageReservation\Mail\CMailPricePackageReservationCreate;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use WebLoader\Nette\CssLoader;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

use function count;
use function explode;
use function intval;
use function is_array;
use function is_bool;
use function sprintf;
use function trim;

/**
 * Class Edit
 */
class FormCreateReservation extends FormWithUserControl
{
    use APackageControl;

    /** @var PricePackageReservationFacade */
    private $facade;

    /** @var PricePackageFacade */
    private $facadePricePackage;

    /** @var LoaderFactory */
    private $webLoader;

    /** @var MailService */
    private $mailService;

    /** @var int|int[]|null */
    private $defaultPackageId = null;

    /** @var int|string|null */
    private $defaultTagId = null;

    /** @var bool */
    private $multiplePackage = false;

    /**
     * @param int|string|null $defaultPackageId
     */
    public function __construct(
        $defaultPackageId,
        $defaultTagId,
        bool $multiplePackage,
        PricePackageReservationFacade $facade,
        PricePackageFacade $facadePricePackage,
        Translator $translator,
        LoaderFactory $webLoader,
        MailService $mailService,
        LoggedUser $user
    ) {
        \Tracy\Debugger::barDump([$defaultPackageId, $defaultTagId]);
        parent::__construct($translator, $user);
        $this->facade = $facade;
        $this->facadePricePackage = $facadePricePackage;
        $this->webLoader = $webLoader;
        $this->mailService = $mailService;

        $this->multiplePackage = $multiplePackage;
        if ($this->multiplePackage) {
            $this->defaultPackageId = Arrays::map(explode(',', (string)$defaultPackageId), static function ($id): int {
                return intval($id);
            });
        } else {
            $this->defaultPackageId = intval($defaultPackageId);
        }
        $this->defaultTagId = $defaultTagId ?? null;
    }

    public function getTitle(): string
    {
        return 'form.price-package-reservation.front.create-reservation.title';
    }

    /**
     * @return CssLoader[]
     */
    public function getCss(): array
    {
        return [$this->webLoader->createCssLoader('daterangePicker')];
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        $jss = [
            $this->webLoader->createJavaScriptLoader('moment'),
            $this->webLoader->createJavaScriptLoader('daterangePicker'),
            $this->webLoader->createJavaScriptLoader('reCaptchaInvisible'),
        ];

        if ($this->multiplePackage) {
            $jss[] = $this->webLoader->createJavaScriptLoader('choosen');
        }

        return $jss;
    }

    public function processOnSuccess(Form $form, ArrayHash $values): void
    {
        /**
         * @var DateTime $termFrom
         * @var DateTime $termTo
         */
        [$termFrom, $termTo] = Arrays::map(explode(' - ', $values->term), static function (string $date): DateTime {
            $date = DateTime::createFromFormat('d.m.Y H:i', $date);
            return is_bool($date) ? new DateTime() : $date;
        });

        // PRICE PACKAGES
        $pricePackages = [];
        foreach ((array)$values->price_package_id as $pricePackageId) {
            $pricePackages[] = $this->facadePricePackage->get($pricePackageId);
        }

        $pricePackageReservation = $this->facade->create(
            $values->degree_before,
            $values->name,
            $values->surname,
            $values->degree_after,
            $values->email,
            $values->phone,
            trim(
                sprintf(
                    '%s %s, %s %s',
                    $values->address_street,
                    $values->address_nod,
                    $values->address_city,
                    $values->address_code
                )
            ),
            $values->address_country,
            $values->is_company ? $values->company_name : '',
            $values->is_company ? $values->company_registration_number : '',
            $values->is_company ? $values->company_tax_id : '',
            $pricePackages,
            $termFrom,
            $termTo,
            $values->place_of_action,
            $values->content
        );

        $pricePackageReservation = $this->facade->updatePublic(
            $pricePackageReservation->getId(),
            $values->type === 'Jiné' ? $values->type_other : $values->type,
            '',
            null,
            []
        );

        $CMailPricePackageReservationCreate = new CMailPricePackageReservationCreate(
            $pricePackageReservation,
            $this->translator
        );

        $mailQueue = $this->mailService->addByTemplateType(
            CMailPricePackageReservationCreate::TYPE,
            $CMailPricePackageReservationCreate,
            [$values->email],
            true
        );

        if ($mailQueue !== null && $mailQueue->getStatus() === MailQueue::STATUS_SENT) {
            $message = new SimpleTranslation(
                'form.price-package-reservation.front.create-reservation.flash.success-mail %s', $values->email
            );
            $this->onFlashmessage($message, Flash::SUCCESS);
        } else {
            $message = new SimpleTranslation(
                'form.price-package-reservation.front.create-reservation.flash.danger %s',
                $values->email
            );
            $this->onFlashmessage($message, Flash::DANGER);
        }

        $this->onSuccess($form, $values, $form->isSubmitted()->name);

        $form->reset();
        $this->redrawControl('snipForm');
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile($this->getControlTemplate(__DIR__ . '/formCreateReservation.latte'));

        $template->pricePackagePrices = $this->facadePricePackage->getPairs('id', 'price');

        $template->render();
    }

    protected function createComponentForm(): Form
    {
        $form = new Form();
        $form->setTranslator($this->translator);

        // Data
        $dataFormPricePackage = [];
        $pricePackageTypes = [0 => ['name' => 'Hlavní nabídka', 'required' => []]];
        foreach ($this->facadePricePackage->getAll(true) as $pricePackage) {
            $typeId = $pricePackage->getType() ? $pricePackage->getType()->getId() : 0;

            if ($pricePackage->getType()) {
                if (!isset($pricePackageTypes[$typeId])) {
                    $pricePackageTypes[$typeId] = [
                        'name' => $pricePackage->getType()->getName(),
                        'required' => [],
                    ];
                }

                if ($pricePackage->getType()->isRequired()) {
                    $pricePackageTypes[$typeId]['required'][] = $pricePackage->getId();
                }
            }

            if (!isset($dataFormPricePackage[$typeId])) {
                $dataFormPricePackage[$typeId] = [];
            }
            $dataFormPricePackage[$typeId][$pricePackage->getId()] = $pricePackage->getName();
        }
        ksort($dataFormPricePackage);
        $tmpData = [];
        foreach ($dataFormPricePackage as $typeId => $formPricePackages) {
            $tmpData[$pricePackageTypes[$typeId]['name']] = $formPricePackages;
        }
        $dataFormPricePackage = $tmpData;

        $invalidDates = $this->getFutureTermReservations();

        // Customer
        $form->addText('degree_before', 'form.price-package-reservation.front.create-reservation.degree-before');
        $form->addText('name', 'form.price-package-reservation.front.create-reservation.name')
            ->setRequired('form.price-package-reservation.front.create-reservation.name.req');
        $form->addText('surname', 'form.price-package-reservation.front.create-reservation.surname')
            ->setRequired('form.price-package-reservation.front.create-reservation.surname.req');
        $form->addText('degree_after', 'form.price-package-reservation.front.create-reservation.degree-after');

        $form->addEmail('email', 'form.price-package-reservation.front.create-reservation.email')
            ->setRequired('form.price-package-reservation.front.create-reservation.email.req');
        $form->addText('phone', 'form.price-package-reservation.front.create-reservation.phone')
            ->setRequired('form.price-package-reservation.front.create-reservation.phone.req');

        $form->addText('address_street', 'form.price-package-reservation.front.create-reservation.address-street')
            ->setRequired('form.price-package-reservation.front.create-reservation.address-street.req');
        $form->addText('address_nod', 'form.price-package-reservation.front.create-reservation.address-nod')
            ->setRequired('form.price-package-reservation.front.create-reservation.address-nod.req');
        $form->addText('address_city', 'form.price-package-reservation.front.create-reservation.address-city')
            ->setRequired('form.price-package-reservation.front.create-reservation.address-city.req');
        $form->addText('address_code', 'form.price-package-reservation.front.create-reservation.address-code')
            ->setRequired('form.price-package-reservation.front.create-reservation.address-code.req');
        $form->addText('address_country', 'form.price-package-reservation.front.create-reservation.address-country')
            ->setRequired('form.price-package-reservation.front.create-reservation.address-country.req');

        // Company
        $inputIsCompany = $form->addCheckbox(
            'is_company',
            'form.price-package-reservation.front.create-reservation.is-company'
        );
        $inputIsCompany->addCondition(Form::EQUAL, true)
            ->toggle('form-company');
        $form->addText('company_name', 'form.price-package-reservation.front.create-reservation.company-name')
            ->addConditionOn($inputIsCompany, Form::EQUAL, true)
            ->setRequired('form.price-package-reservation.front.create-reservation.company-name.req');
        $form->addText(
            'company_registration_number',
            'form.price-package-reservation.front.create-reservation.company-registration-number'
        )
            ->setHtmlAttribute(
                'placeholder',
                'form.price-package-reservation.front.create-reservation.phone.placeholder'
            )
            ->addConditionOn($inputIsCompany, Form::EQUAL, true)
            ->setRequired('form.price-package-reservation.front.create-reservation.company-registration-number.req');
        $form->addText('company_tax_id', 'form.price-package-reservation.front.create-reservation.company-tax-id');

        // Package
        if ($this->multiplePackage) {
            $form->addMultiSelect(
                'price_package_id',
                'form.price-package-reservation.front.create-reservation.price-package-id',
                $dataFormPricePackage
            )
                ->setTranslator(null)
                ->setRequired('form.price-package-reservation.front.create-reservation.price-package-id.req')
                ->setHtmlAttribute(
                    'data-chosen-placeholder-text',
                    $this->translator->translate('form.price-package-reservation.edit.price-package-id.placeholder')
                )
                ->setHtmlAttribute(
                    'data-chosen-no-result-text',
                    $this->translator->translate('form.price-package-reservation.edit.price-package-id.no-result')
                )
                ->setHtmlAttribute(
                    'data-frm-required-types',
                    json_encode(array_filter($pricePackageTypes, fn($type) => count($type['required']) > 0))
                );
        } else {
            $form->addSelect(
                'price_package_id',
                'form.price-package-reservation.front.create-reservation.price-package-id',
                $dataFormPricePackage
            )
                ->setTranslator(null)
                ->setRequired('form.price-package-reservation.front.create-reservation.price-package-id.req')
                ->setPrompt(Constant::PROMTP)
                ->setHtmlAttribute(
                    'data-frm-required-types',
                    json_encode(array_filter($pricePackageTypes, fn($type) => count($type['required']) > 0))
                );
        }

        $form->addSelect('type', 'form.price-package-reservation.front.create-reservation.type', [
            null => '-- Vyberte --',
            'Narozeniny' => 'Narozeniny',
            'Svatba' => 'Svatba',
            'Firemní večírek' => 'Firemní večírek',
            'Ples' => 'Ples',
            'Jiné' => 'Jiné',
        ])->setRequired('form.price-package-reservation.front.create-reservation.type.req')
            ->setTranslator(null)
            ->addCondition(Form::EQUAL, 'Jiné')
            ->toggle('frm-toggle-type-other');

        $form->addText('type_other', 'form.price-package-reservation.front.create-reservation.type-other')
            ->addConditionOn($form['type'], Form::EQUAL, 'Jiné')
            ->setRequired('form.price-package-reservation.front.create-reservation.type-other.req');

        $form->addText('term', 'form.price-package-reservation.front.create-reservation.term')
            ->setHtmlAttribute('data-daterange', 'DD.MM.YYYY HH:mm')
            ->setHtmlAttribute('data-daterange-timepicker')
            ->setHtmlAttribute('data-daterange-invalid-dates', json_encode($invalidDates))
            ->addRule(function (TextInput $term) use ($invalidDates): bool {
                [$termFrom, $termTo] = Arrays::map(
                    explode(' - ', $term->getValue()),
                    static function (string $date): DateTime {
                        $date = DateTime::createFromFormat('d.m.Y H:i', $date);
                        return is_bool($date) ? new DateTime() : $date;
                    }
                );

                $invalidDates = $this->getFutureTermReservations();
                $date = clone $termFrom;
                while ($date->format('Ymd') <= $termTo->format('Ymd')) {
                    foreach ($invalidDates as $invalidDate) {
                        $dateFrom = DateTime::createFromFormat('Y-m-d', $invalidDate['from']);
                        $dateTo = DateTime::createFromFormat('Y-m-d', $invalidDate['to']);

                        if (
                            $date->format('Ymd') >= $dateFrom->format('Ymd')
                            && $date->format('Ymd') <= $dateTo->format('Ymd')
                        ) {
                            return false;
                        }
                    }

                    $date->modify('+1 day');
                }

                return true;
            }, 'form.price-package-reservation.front.create-reservation.term.invalid');
        $form->addText('place_of_action', 'form.price-package-reservation.front.create-reservation.place-of-action')
            ->setRequired('form.price-package-reservation.front.create-reservation.place-of-action.req');
        $form->addTextArea('content', 'form.price-package-reservation.front.create-reservation.content', null, 10);

        // Assent
        $assentText = $this->translator->translate(
            'form.price-package-reservation.front.create-reservation.assent [link]'
        );
        $e_assentText = explode('[link]', $assentText);
        $assentHtml = new Html();
        if (count($e_assentText) === 2) {
            $assentLink = Html::el('a', [
                'href' => $this->translator->translate(
                    'form.price-package-reservation.front.create-reservation.assent.link.href'
                ),
                'target' => '_blank',
                'class' => $this->translator->translate(
                    'form.price-package-reservation.front.create-reservation.assent.link.class'
                ),
            ])->setText(
                $this->translator->translate('form.price-package-reservation.front.create-reservation.assent.link')
            );
            $assentHtml->addText($e_assentText[0])
                ->addHtml($assentLink)
                ->addText($e_assentText[1]);
        } else {
            $assentHtml->addText($assentText);
        }
        $form->addCheckbox('assent', $assentHtml)
            ->setTranslator(null)
            ->setRequired(
                $this->translator->translate('form.price-package-reservation.front.create-reservation.assent.req')
            );

        // CAPTCHA
        $form->addInvisibleReCaptchaInput();

        // BUTTON
        $form->addSubmit('send', 'form.price-package-reservation.front.create-reservation.send');

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        if ($this->defaultPackageId !== null) {
            $allDataFormPricePackage = [];
            foreach ($dataFormPricePackage as $formPricePackages) {
                $allDataFormPricePackage = $allDataFormPricePackage + $formPricePackages;
            }

            if ($this->multiplePackage && is_array($this->defaultPackageId)) {
                $pricePackagesId = [];
                foreach ($this->defaultPackageId as $packageId) {
                    if (isset($allDataFormPricePackage[$packageId])) {
                        $pricePackagesId[] = $packageId;
                    }
                }
                $form->setDefaults(['price_package_id' => array_filter($pricePackagesId)]);
            } else {
                if (isset($allDataFormPricePackage[$this->defaultPackageId])) { //@phpstan-ignore-line
                    $form->setDefaults(['price_package_id' => $this->defaultPackageId]);
                }
            }
        }

        if ($this->defaultTagId !== null) {
            $form->setDefaults(['type' => $this->defaultTagId]);
        }

        return $form;
    }

    public function handleLoadFromAres(?string $val = null): void
    {
        $ares = (new AresFactory())->create();
        try {
            $aresData = $ares->loadBasic(preg_replace('/\s+/', '', $val));

            $this['form']['company_name']->setValue($aresData->company);
            $this['form']['company_registration_number']->setValue($aresData->in);
            $this['form']['company_tax_id']->setValue($aresData->tin);

            $this->redrawControl('snipForm');
            $this->redrawControl('snipFormcompany');
        } catch (\Exception $e) {
            $presenter = $this->getPresenterIfExists();
            if ($presenter !== null) {
                $presenter->flashMessage(
                    'form.price-package-reservation.front.create-reservation.flash.info.invalid-company-id',
                    Flash::WARNING
                );
            }
        }
    }

    /**
     * @return array<int, array{from: string, to: string}>
     */
    protected function getFutureTermReservations(): array
    {
        $futureReservations = [];
        foreach ($this->facade->getFutureReservations() as $futureReservation) {
            $futureReservations[] = [
                'from' => $futureReservation->getTermFrom()->format('Y-m-d'),
                'to' => $futureReservation->getTermTo()->format('Y-m-d'),
            ];
        }

        return $futureReservations;
    }
}
