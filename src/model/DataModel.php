<?php

declare(strict_types=1);

namespace Skadmin\PricePackageReservation\Model;

use Exception;
use Nette\Utils\DateTime;
use function abs;
use function array_keys;
use function count;
use function date;
use function intval;
use function is_bool;
use function sprintf;

class DataModel
{
    /**
     * @return mixed[]
     */
    public static function prepareDataForMonth(DateTime $date) : array
    {
        $prevMonth = self::getPrevMonth($date);
        $nextMonth = self::getNextMonth($date);

        // chceck first day in month - 1 (for Monday) through 7 (for Sunday)
        $firstDayMonth = intval($date->format('N'));

        return self::buildCalendarData($firstDayMonth, $prevMonth, $date, $nextMonth);
    }

    /**
     * @param int|string|null $_month
     * @param int|string|null $_year
     */
    public static function getMonth($_month, $_year) : DateTime
    {
        $month = $_month !== null ? intval($_month) : intval(date('n'));
        $year  = $_year !== null ? intval($_year) : intval(date('Y'));

        $date = DateTime::createFromFormat('Ymd', sprintf('%d%02d01', $year, $month));
        return is_bool($date) ? new DateTime() : $date;
    }

    /**
     * @param mixed[] $data
     *
     * @return mixed[]
     */
    public static function extractFirstAndLastDayFromDataForMonth(array $data) : array
    {
        $firstWeek = array_keys($data[0]);
        $lastWeek  = array_keys($data[count($data) - 1]);

        $firstDay = DateTime::createFromFormat('Ymd', (string) $firstWeek[0]);
        $lastDay  = DateTime::createFromFormat('Ymd', (string) $lastWeek[6]);

        if (is_bool($firstDay) || is_bool($lastDay)) {
            throw new Exception('Invalide first or last day');
        }

        $firstDay->setTime(0, 0, 0);
        $lastDay->setTime(23, 59, 59);

        return [$firstDay, $lastDay];
    }

    private static function getPrevMonth(DateTime $month) : DateTime
    {
        $prevMonth = clone $month;
        $prevMonth->modify('-1 month');
        $prevMonth->modify('last day of this month');

        return $prevMonth;
    }

    private static function getNextMonth(DateTime $month) : DateTime
    {
        $nextMonth = clone $month;
        $nextMonth->modify('+1 month');
        $nextMonth->modify('first day of this month');

        return $nextMonth;
    }

    /**
     * @return mixed[]
     */
    private static function buildCalendarData(int $firstDayMonth, DateTime $prevMonth, DateTime $currentMonth, DateTime $nextMonth) : array
    {
        // calculate first day in same row for prev month - last day prev month + 2 as coeficient mondey - first day in current week
        $firstDayWeekPrevMonth = intval($prevMonth->format('d')) + 2 - $firstDayMonth;

        // prepare calendar
        $daysForCalendar = [];
        $week            = 0;

        // add prev month
        self::preparePrevMonth($daysForCalendar, $week, $firstDayMonth, $firstDayWeekPrevMonth, $prevMonth);

        // add curr month
        self::prepareCurrMonth($daysForCalendar, $week, $firstDayMonth, $currentMonth);

        // add next month
        self::prepareNextMonth($daysForCalendar, $week, $nextMonth);

        return $daysForCalendar;
    }

    /**
     * @param mixed[] $daysForCalendar
     */
    private static function preparePrevMonth(array &$daysForCalendar, int &$week, int $firstDayMonth, int $firstDayWeekPrevMonth, DateTime $prevMonth) : void
    {
        for ($i = 0; $i < abs(1 - $firstDayMonth); $i++) {
            self::addDay($daysForCalendar, $week, $prevMonth, $firstDayWeekPrevMonth++, 'event-calendar-off');
        }
    }

    /**
     * @param mixed[] $daysForCalendar
     */
    private static function prepareCurrMonth(array &$daysForCalendar, int &$week, int $firstDayMonth, DateTime $currentMonth) : void
    {
        $days = intval($currentMonth->format('t'));
        $day  = 1;
        for ($i = $firstDayMonth; $i < $days + $firstDayMonth; $i++) {
            self::addDay($daysForCalendar, $week, $currentMonth, $day++);

            if ($i % 7 !== 0) {
                continue;
            }

            $week++;
        }
    }

    /**
     * @param mixed[] $daysForCalendar
     */
    private static function prepareNextMonth(array &$daysForCalendar, int &$week, DateTime $nextMonth) : void
    {
        if (! isset($daysForCalendar[$week])) {
            return;
        }

        $day = 1;
        for ($i = count($daysForCalendar[$week]); $i < 7; $i++) {
            self::addDay($daysForCalendar, $week, $nextMonth, $day++, 'event-calendar-off');
        }
    }

    /**
     * @param mixed[] $daysForCalendar
     */
    private static function addDay(array &$daysForCalendar, int $week, DateTime $month, int $day, string $class = '') : void
    {
        $daysForCalendar[$week][self::keyDay($month, $day)] = [
            'day'   => $day,
            'class' => $class,
        ];
    }

    private static function keyDay(DateTime $monthYear, int $day) : string
    {
        return sprintf('%s%02d', $monthYear->format('Ym'), $day);
    }
}
