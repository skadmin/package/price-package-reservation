<?php

declare(strict_types=1);

namespace Skadmin\PricePackageReservation\Model;

use Nette\Utils\DateTime;
use Skadmin\PricePackageReservation\Doctrine\PricePackageReservation\PricePackageReservation;
use Skadmin\PricePackageReservation\Doctrine\PricePackageReservation\PricePackageReservationFacade;

class Export
{
    /** @var PricePackageReservationFacade */
    private $facade;

    public function __construct(PricePackageReservationFacade $facade)
    {
        $this->facade = $facade;
    }

    /**
     * @return array<PricePackageReservation>
     */
    public function getReservationsForCalendarMonth(DateTime $dateTime) : array
    {
        $calendarRange        = DataModel::prepareDataForMonth($dateTime);
        [$firstDay, $lastDay] = DataModel::extractFirstAndLastDayFromDataForMonth($calendarRange);

        return $this->facade->getBetweenDates($firstDay, $lastDay, true);
    }
}
