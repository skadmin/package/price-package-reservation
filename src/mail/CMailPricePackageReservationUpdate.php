<?php

declare(strict_types=1);

namespace Skadmin\PricePackageReservation\Mail;

use Skadmin\Mailing\Model\CMail;
use Skadmin\Mailing\Model\MailParameterValue;
use Skadmin\Mailing\Model\MailTemplateParameter;
use App\Model\System\Strings;
use DateTimeInterface;
use Haltuf\Genderer\Genderer;
use ReflectionProperty;
use Skadmin\PricePackageReservation\Doctrine\PricePackageReservation\PricePackageReservation;
use Skadmin\Translator\Translator;
use function call_user_func;
use function implode;
use function method_exists;
use function number_format;
use function sprintf;

class CMailPricePackageReservationUpdate extends CMail
{
    public const TYPE = 'price-package-reservation-update';

    /** @var string */
    private $name;

    /** @var string */
    private $vocativName;

    /** @var string */
    private $email;

    /** @var string */
    private $phone;

    /** @var string */
    private $address;

    /** @var string */
    private $addressCountry;

    /** @var string */
    private $status;

    /** @var string */
    private $code;

    /** @var string */
    private $companyName;

    /** @var string */
    private $companyRegistrationNumber;

    /** @var string */
    private $companyTaxId;

    /** @var string */
    private $pricePackageName;

    /** @var string */
    private $pricePackagePrice;

    /** @var string */
    private $pricePackageContent;

    /** @var string */
    private $term;

    /** @var string */
    private $placeOfAction;

    /** @var string */
    private $content;

    /** @var DateTimeInterface */
    private $createdAt;

    public function __construct(PricePackageReservation $pricePackageReservation, Translator $translator)
    {
        $genderer = new Genderer();

        $this->name           = $pricePackageReservation->getFullName();
        $this->vocativName    = $genderer->getVocative($pricePackageReservation->getFullName());
        $this->email          = $pricePackageReservation->getEmail();
        $this->phone          = $pricePackageReservation->getPhone();
        $this->address        = $pricePackageReservation->getAddress();
        $this->addressCountry = $pricePackageReservation->getAddressCountry();

        $this->status = $translator->translate($pricePackageReservation->getStatusText());
        $this->code   = $pricePackageReservation->getCode();

        $this->companyName               = $pricePackageReservation->getCompanyName() !== '' ? $pricePackageReservation->getCompanyName() : '--';
        $this->companyRegistrationNumber = $pricePackageReservation->getCompanyRegistrationNumber() !== '' ? $pricePackageReservation->getCompanyRegistrationNumber() : '--';
        $this->companyTaxId              = $pricePackageReservation->getCompanyTaxId() !== '' ? $pricePackageReservation->getCompanyTaxId() : '--';

        $names = [];
        foreach ($pricePackageReservation->getPricePackages() as $pricePackage) {
            $names[] = $pricePackage->getName();
        }
        $this->pricePackageName    = implode(', ', $names);
        $this->pricePackagePrice   = number_format($pricePackageReservation->getPricePackagePrice(), 2, ',', ' ');
        $this->pricePackageContent = $pricePackageReservation->getPricePackageContent();

        $this->term          = $pricePackageReservation->getTermWithTimeClever();
        $this->placeOfAction = $pricePackageReservation->getPlaceOfAction();
        $this->content = implode(
            ' || ',
            array_filter([$pricePackageReservation->getContent(), $pricePackageReservation->getDescription()])
        );

        $this->createdAt = $pricePackageReservation->getCreatedAt();
    }

    /**
     * @return mixed[]
     */
    public static function getModelForSerialize() : array
    {
        $model = [];

        /** @var ReflectionProperty $property */
        foreach (self::getProperties(self::class) as $property) {
            $description = sprintf('mail.price-package-reservation-update.parameter.%s.description', $property);
            $example     = sprintf('mail.price-package-reservation-update.parameter.%s.example', $property);

            $model[] = (new MailTemplateParameter($property, $description, $example))->getDataForSerialize();
        }

        return $model;
    }

    /**
     * @return MailParameterValue[]
     */
    public function getParameterValues() : array
    {
        $mailParameterValue = [];

        /** @var ReflectionProperty $property */
        foreach (self::getProperties(self::class) as $property) {
            $method = sprintf('get%s', Strings::camelize($property));

            if (method_exists($this, $method)) {
                $mailParameterValue[] = new MailParameterValue($property, call_user_func([$this, $method]));
            }
        }

        return $mailParameterValue;
    }

    public function getName() : string
    {
        return $this->name;
    }

    public function getVocativName() : string
    {
        return $this->vocativName;
    }

    public function getEmail() : string
    {
        return $this->email;
    }

    public function getPhone() : string
    {
        return $this->phone;
    }

    public function getAddress() : string
    {
        return $this->address;
    }

    public function getAddressCountry() : string
    {
        return $this->addressCountry;
    }

    public function getStatus() : string
    {
        return $this->status;
    }

    public function getCode() : string
    {
        return $this->code;
    }

    public function getCompanyName() : string
    {
        return $this->companyName;
    }

    public function getCompanyRegistrationNumber() : string
    {
        return $this->companyRegistrationNumber;
    }

    public function getCompanyTaxId() : string
    {
        return $this->companyTaxId;
    }

    public function getPricePackageName() : string
    {
        return $this->pricePackageName;
    }

    public function getPricePackagePrice() : string
    {
        return $this->pricePackagePrice;
    }

    public function getPricePackageContent() : string
    {
        return $this->pricePackageContent;
    }

    public function getTerm() : string
    {
        return $this->term;
    }

    public function getPlaceOfAction() : string
    {
        return $this->placeOfAction;
    }

    public function getContent() : string
    {
        return $this->content;
    }

    public function getCreatedAt() : DateTimeInterface
    {
        return $this->createdAt;
    }
}
