<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210316144900 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $translations = [
            ['original' => 'grid.price-package-reservation.overview.tags', 'hash' => '64819d64ba825b1d1543baca23b9b36a', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Štítek', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.price-package-reservation.overview.action.overview-tag', 'hash' => '22c6e84a32aa177fa75e814cad879982', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Přehled štítků', 'plural1' => '', 'plural2' => ''],
            ['original' => 'price-package-reservation.overview-tag.title', 'hash' => '34f84d3287477b1b90828c2f0d4459a3', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Štítky RCB', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.price-package-reservation.overview-tag.overview.action.inline-add', 'hash' => 'e67748fc453be4205295e77a65f921de', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založit štítek RCB', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.price-package-reservation.overview-tag.overview.name.req', 'hash' => 'a4947a25f9d124281b54ba18cd2eb94e', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zadejte prosím název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.price-package-reservatio.overview-tag.action.overview', 'hash' => '6b79e601dcbfbbed6d9ed195660f872a', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Přehled rezervací CB', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.price-package-reservation.overview-tag.name', 'hash' => '80e618f026aad3998e561bd4106a7afc', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.price-package-reservation.overview-tag.color', 'hash' => 'd6b088836ff61ac756502e6dfb519e74', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Barva', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.price-package-reservation.overview-tag.is-active', 'hash' => 'e8552e336c6dc1976de1c32558e78180', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Je aktivní?', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.price-package-reservation.overview-tag.overview.action.flash.inline-add.success "%s"', 'hash' => 'deb4be8717b3e77747c56f7fd1384eaa', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Štítek "%s" RCB byl úspěšně založen.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.price-package-reservation.overview-tag.overview.action.flash.inline-edit.success "%s"', 'hash' => 'c03820e012274f8c72ef08cccb353595', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Štítek "%s" RCB byl úspěšně upraven.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.price-package-reservation.overview-tag.action.flash.sort.success', 'hash' => '6c022fc7ffa2122e7ff94546b91ef0c2', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Pořadí štítků RCB bylo upraveno.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.price-package-reservation.front.create-reservation.section.base', 'hash' => '62421c7db6d944a1258b942b2ee4a047', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Základní údaje', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.price-package-reservation.edit.title', 'hash' => 'cd63384f64000e3d4d9b89329ffc8331', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.price-package-reservation.edit.image-preview', 'hash' => '7b8382d6c305c5704166215908566ab9', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Náhled rezervace', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.price-package-reservation.edit.image-preview.rule-image', 'hash' => '427cd0360cbb45db0494841c82d05f01', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Vybraný soubor není platný obrázek', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.price-package-reservation.edit.tags', 'hash' => '910852436c7550a5c3089c819d19d880', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Štítky', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.price-package-reservation.edit.tags.placeholder', 'hash' => '7cea354e24c7c2367c784d8cccf6303b', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Začněte psát pro vyhledávání...', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.price-package-reservation.edit.tags.no-result', 'hash' => '331476662d350047e3643393b602e88a', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Štítek nebyl nalezen:', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.price-package-reservation.edit.note', 'hash' => '29cd493026dfaf2c12a97f3f26154d28', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Veřejná poznámka', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.price-package-reservation.edit.price-package-id.placeholder', 'hash' => 'b7055c9dd76dc359da38239adf4dc900', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Začněte psát pro vyhledávání...', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.price-package-reservation.edit.price-package-id.no-result', 'hash' => 'e3e717b71f45a05f1c62b34add04da39', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Balíček nebyl nalezen:', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.price-package-reservation.edit.flash.registration-update.danger %s', 'hash' => '39691a3b388e96e253a9547c444ebf48', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Nepodařilo se odeslat e-mail na "%s"', 'plural1' => '', 'plural2' => ''],
        ];

        foreach ($translations as $translation) {
            $this->addSql('DELETE FROM translation WHERE hash = :hash', $translation);
            $this->addSql('SELECT create_translation(:original, :hash, :module, :language_id, :singular, :plural1, :plural2)', $translation);
        }
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
