<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Skadmin\PricePackageReservation\BaseControl;
use function serialize;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210206201647 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE price_package_reservation_rel_price_package (price_package_reservation_id INT NOT NULL, price_package_id INT NOT NULL, INDEX IDX_A73261FB25E0C6E2 (price_package_reservation_id), INDEX IDX_A73261FB40C4A4FB (price_package_id), PRIMARY KEY(price_package_reservation_id, price_package_id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE price_package_reservation_rel_price_package_reservation_tag (price_package_reservation_id INT NOT NULL, price_package_reservation_tag_id INT NOT NULL, INDEX IDX_2D0A7E4825E0C6E2 (price_package_reservation_id), INDEX IDX_2D0A7E489533D65B (price_package_reservation_tag_id), PRIMARY KEY(price_package_reservation_id, price_package_reservation_tag_id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE price_package_reservation_tag (id INT AUTO_INCREMENT NOT NULL, color VARCHAR(255) NOT NULL, is_active TINYINT(1) DEFAULT \'1\' NOT NULL, webalize VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, sequence INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE price_package_reservation_rel_price_package ADD CONSTRAINT FK_A73261FB25E0C6E2 FOREIGN KEY (price_package_reservation_id) REFERENCES price_package_reservation (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE price_package_reservation_rel_price_package ADD CONSTRAINT FK_A73261FB40C4A4FB FOREIGN KEY (price_package_id) REFERENCES price_package (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE price_package_reservation_rel_price_package_reservation_tag ADD CONSTRAINT FK_2D0A7E4825E0C6E2 FOREIGN KEY (price_package_reservation_id) REFERENCES price_package_reservation (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE price_package_reservation_rel_price_package_reservation_tag ADD CONSTRAINT FK_2D0A7E489533D65B FOREIGN KEY (price_package_reservation_tag_id) REFERENCES price_package_reservation_tag (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE price_package_reservation DROP FOREIGN KEY FK_317DE56540C4A4FB');
        $this->addSql('DROP INDEX IDX_317DE56540C4A4FB ON price_package_reservation');
        $this->addSql('ALTER TABLE price_package_reservation ADD note VARCHAR(255) NOT NULL, ADD image_preview VARCHAR(255) DEFAULT NULL, ADD title VARCHAR(255) NOT NULL, DROP price_package_id, CHANGE price_package_content price_package_content LONGTEXT NOT NULL');

        $data = [
            'additional_privilege' => serialize(BaseControl::ADDITIONAL_PRIVILEGE),
            'name'                 => BaseControl::RESOURCE,
        ];
        $this->addSql('UPDATE resource SET additional_privilege = :additional_privilege WHERE name = :name', $data);
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE price_package_reservation_rel_price_package_reservation_tag DROP FOREIGN KEY FK_2D0A7E489533D65B');
        $this->addSql('DROP TABLE price_package_reservation_rel_price_package');
        $this->addSql('DROP TABLE price_package_reservation_rel_price_package_reservation_tag');
        $this->addSql('DROP TABLE price_package_reservation_tag');
        $this->addSql('ALTER TABLE price_package_reservation ADD price_package_id INT DEFAULT NULL, DROP note, DROP image_preview, DROP title, CHANGE price_package_content price_package_content VARCHAR(255) CHARACTER SET utf8 NOT NULL COLLATE `utf8_unicode_ci`');
        $this->addSql('ALTER TABLE price_package_reservation ADD CONSTRAINT FK_317DE56540C4A4FB FOREIGN KEY (price_package_id) REFERENCES price_package (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_317DE56540C4A4FB ON price_package_reservation (price_package_id)');
    }
}
