<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Nette\Utils\DateTime;
use Skadmin\PricePackageReservation\Mail\CMailPricePackageReservationCreate;
use Skadmin\PricePackageReservation\Mail\CMailPricePackageReservationUpdate;
use function serialize;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200424124841 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $mailTemplates = [
            [
                'content'            => '',
                'parameters'         => serialize(CMailPricePackageReservationCreate::getModelForSerialize()),
                'type'               => CMailPricePackageReservationCreate::TYPE,
                'class'              => CMailPricePackageReservationCreate::class,
                'name'               => 'mail.price-package-reservation-create.name',
                'subject'            => 'mail.price-package-reservation-create.subject',
                'last_update_author' => 'Cron',
                'last_update_at'     => new DateTime(),
            ],
            [
                'content'            => '',
                'parameters'         => serialize(CMailPricePackageReservationUpdate::getModelForSerialize()),
                'type'               => CMailPricePackageReservationUpdate::TYPE,
                'class'              => CMailPricePackageReservationUpdate::class,
                'name'               => 'mail.price-package-reservation-update.name',
                'subject'            => 'mail.price-package-reservation-update.subject',
                'last_update_author' => 'Cron',
                'last_update_at'     => new DateTime(),
            ],
        ];

        foreach ($mailTemplates as $mailTemplate) {
            $this->addSql(
                'INSERT INTO mail_template (content, parameters, type, class, name, subject, last_update_author, last_update_at) VALUES (:content, :parameters, :type, :class, :name, :subject, :last_update_author, :last_update_at)',
                $mailTemplate
            );
        }
    }

    public function down(Schema $schema) : void
    {
    }
}
