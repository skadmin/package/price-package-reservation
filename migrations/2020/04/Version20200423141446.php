<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200423141446 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE price_package_reservation (id INT AUTO_INCREMENT NOT NULL, price_package_id INT DEFAULT NULL, degree_before VARCHAR(255) NOT NULL, degree_after VARCHAR(255) NOT NULL, address_country VARCHAR(255) NOT NULL, company_name VARCHAR(255) NOT NULL, company_registration_number VARCHAR(255) NOT NULL, company_tax_id VARCHAR(255) NOT NULL, place_of_action VARCHAR(255) NOT NULL, surname VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, address VARCHAR(255) NOT NULL, phone VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, content LONGTEXT NOT NULL, term_from DATETIME NOT NULL, term_to DATETIME NOT NULL, is_active TINYINT(1) DEFAULT \'1\' NOT NULL, created_at DATETIME NOT NULL, INDEX IDX_317DE56540C4A4FB (price_package_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE price_package_reservation ADD CONSTRAINT FK_317DE56540C4A4FB FOREIGN KEY (price_package_id) REFERENCES price_package (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE price_package_reservation');
    }
}
