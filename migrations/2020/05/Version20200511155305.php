<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200511155305 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $translations = [
            ['original' => 'price-package-reservation.edit.title - %s', 'hash' => '1e7e42702e88d40cf5f0ab4b223f53d2', 'module' => 'admin', 'language_id' => 1, 'singular' => '%s|Editace rezervace ceníkového balíčku', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.price-package-reservation.front.create-reservation.section.customer', 'hash' => '3d37c2a7b203a502e71b1f729d2d6c1a', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zákazník', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.price-package-reservation.edit.degree-before', 'hash' => '02ed4cca416fef3a2bdaca9eb48c18d8', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Titul před', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.price-package-reservation.edit.name', 'hash' => 'a5b67099c1b06e52ec5212875f496919', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Jméno', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.price-package-reservation.edit.name.req', 'hash' => '7b038eb83f6b502c09f3e9c99b9620af', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zadejte prosím jméno', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.price-package-reservation.edit.surname', 'hash' => '2825fc944ee62cf0c4129b2f1ccd7870', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Příjmení', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.price-package-reservation.edit.surname.req', 'hash' => 'eb838c43101ff2e78cb9974c1989f8af', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zadejte prosím příjmení', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.price-package-reservation.edit.degree-after', 'hash' => '5a724983393cf812a1419de2de00d884', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Titul za', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.price-package-reservation.edit.email', 'hash' => '1cc7c03d12a3868f4402cebec3073be1', 'module' => 'admin', 'language_id' => 1, 'singular' => 'E-mail', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.price-package-reservation.edit.email.req', 'hash' => 'dd2fd05c3ef20400bd5fdb3827ff94c1', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zadejte prosím e-mail', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.price-package-reservation.edit.phone', 'hash' => '223c84d088669ea40649c7a4c2931586', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Telefon', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.price-package-reservation.edit.phone.req', 'hash' => 'bff954651abad62d84260793b6bb4757', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zadejte prosím telefon', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.price-package-reservation.edit.address-country', 'hash' => 'abffaad7e8a704b081640ac2d76d14a7', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Země', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.price-package-reservation.edit.address-country.req', 'hash' => '75a527bcd4e73d97e8d7e6a60fbc3d38', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zadejte prosím zemi', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.price-package-reservation.front.create-reservation.section.company', 'hash' => '4eeaf53fa18cbd7d37ba15c3f8873cf7', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Společnost', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.price-package-reservation.edit.is-company', 'hash' => '1f5217fe508babf40dbf5c0f8ddf8fec', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zadat údaje o společnosti', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.price-package-reservation.edit.company-registration-number', 'hash' => '307aea31a742f57db78c82710a0a5bfe', 'module' => 'admin', 'language_id' => 1, 'singular' => 'IČO', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.price-package-reservation.edit.company-registration-number.req', 'hash' => '124956e6ab3aad00c4d02804f9224989', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zadejte prosím IČO', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.price-package-reservation.edit.phone.placeholder', 'hash' => 'e637532f0478d046fa8f39c26ad32294', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Podle IČO doplníme údaje', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.price-package-reservation.edit.company-tax-id', 'hash' => '6c80ddf0b49accde63d5e8e0cc48872d', 'module' => 'admin', 'language_id' => 1, 'singular' => 'DIČ', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.price-package-reservation.edit.company-name', 'hash' => '548d384f0be3bd220614874bd7c34b3d', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.price-package-reservation.edit.company-name.req', 'hash' => '9905eafa9110770d9e00ee9dc3a3be34', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zadejte prosím název společnosti', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.price-package-reservation.front.create-reservation.section.service', 'hash' => '6400bb81900a44bdcc794d8b64519112', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Balíček', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.price-package-reservation.edit.price-package-id', 'hash' => '9be9e74b5cad86b67717d26a7ce7a04d', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Balíček', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.price-package-reservation.edit.price-package-id.req', 'hash' => 'c10374fc33c194e8046a7bd30689c082', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Vyberte prosím balíček', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.price-package-reservation.edit.term', 'hash' => 'bbdc551d486888815aeee4520aab51e6', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Termín', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.price-package-reservation.edit.place-of-action', 'hash' => 'fc07ff881742b1b9387d710ee8d962d7', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Místo či webová stránka události', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.price-package-reservation.edit.description', 'hash' => 'fc17d228acd9924c278c8ae77b857203', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Poznámka admina', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.price-package-reservation.edit.send', 'hash' => '4e43e4f8acbf51bd9cddd77431588538', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.price-package-reservation.edit.send-back', 'hash' => '5488b76ca0133b877f408e5cba039d3d', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit a zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.price-package-reservation.edit.back', 'hash' => '944b3ef0bb1ed4825156f50592c1ee32', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.price-package-reservation.edit.address', 'hash' => '23f79c4560079612b6ebc0bc01bf5087', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Adresa', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.price-package-reservation.edit.address.req', 'hash' => '67e777f4c82497602cb13334da51ac24', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zadejte prosím adresu', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.price-package-reservation.edit.content', 'hash' => 'd64bfe6e456a6f7d03ebf9f04bc3e660', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Poznámka zákazníka', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.price-package-reservation.edit.flash.registration-update.success %s', 'hash' => '64adae4e8687b0fc320dc16ca38177f8', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Byl odeslán informační e-mail na "%s".', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.price-package-reservation.edit.status', 'hash' => 'c6c20ef7943503d31e66697d1c4f8341', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Status', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.price-package-reservation.edit.flash.success.update', 'hash' => '370a366be9156fa6572c819bc10c839d', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Rezervace ceníkového balíčku byla úspěšně upravena.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.price-package-reservation.overview.action.edit', 'hash' => '1db33627696aa5955306bd2cc24defc1', 'module' => 'admin', 'language_id' => 1, 'singular' => '', 'plural1' => '', 'plural2' => ''],
        ];

        foreach ($translations as $translation) {
            $this->addSql('DELETE FROM translation WHERE hash = :hash', $translation);
            $this->addSql('SELECT create_translation(:original, :hash, :module, :language_id, :singular, :plural1, :plural2)', $translation);
        }
    }

    public function down(Schema $schema) : void
    {
    }
}
