<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Nette\Utils\DateTime;
use Skadmin\PricePackageReservation\Mail\CMailPricePackageReservationCancel;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20250119143540 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $mailTemplates = [
            [
                'content'            => '',
                'parameters'         => serialize(CMailPricePackageReservationCancel::getModelForSerialize()),
                'type'               => CMailPricePackageReservationCancel::TYPE,
                'class'              => CMailPricePackageReservationCancel::class,
                'name'               => 'mail.price-package-reservation-cancel.name',
                'subject'            => 'mail.price-package-reservation-cancel.subject',
                'last_update_author' => 'Cron',
                'last_update_at'     => (new DateTime())->format('Y-m-d H:i:s'),
            ],
        ];

        foreach ($mailTemplates as $mailTemplate) {
            $this->addSql(
                'INSERT INTO mail_template (content, parameters, type, class, name, subject, last_update_author, last_update_at) VALUES (:content, :parameters, :type, :class, :name, :subject, :last_update_author, :last_update_at)',
                $mailTemplate
            );
        }
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
